#!/bin/bash
eclipse_dir=/Applications/Anwendungsprogramme/Entwicklung/eclipse
main_dir=${HOME}/Development/Mapasuta/Application/layers/persistence/target/metamodel
metamodel=${HOME}/Development/Mapasuta/Application/models/metamodel/src/main/model/metapersistence.ecore
eclipse_workspace=${HOME}/Development/workspace

mkdir -p $main_dir/src/main/java
java -classpath $eclipse_dir/startup.jar org.eclipse.core.launcher.Main -clean -data $eclipse_workspace -application org.eclipse.emf.importer.ecore.Ecore2GenModel $metamodel $main_dir/metapersistence.genmodel -modelProject $main_dir $main_dir/src/main/java -modelPluginID metapersistence.model -copyright "Copyright (c) Dominik Winter MMVI" -package http://mapasuta.org/entities/mapasuta/v0.1 mapasuta Mapasuta 

java -classpath $eclipse_dir/startup.jar org.eclipse.core.launcher.Main -clean -data $eclipse_workspace -application org.eclipse.emf.codegen.ecore.Generator -model $main_dir/metapersistence.genmodel