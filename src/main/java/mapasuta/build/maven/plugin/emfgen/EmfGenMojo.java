/**
 * Mapasuta
 *
 * Copyright (C) 2006 Dominik Winter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 **/
package mapasuta.build.maven.plugin.emfgen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.StringUtils;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.codehaus.plexus.util.cli.Commandline;

/**
 * This mojo generates the EMF GenModel and the model sources from an ecore
 * file.
 * 
 * @author Dominik Winter
 * @goal genmodel
 * @phase generate-sources
 */
public class EmfGenMojo extends AbstractMojo
{
   /**
    * Installation path to Eclipse
    * 
    * @parameter expression="${eclipse.home}"
    * @required
    */
   private String eclipseDir = null;

   /**
    * Eclipse workspace path, <bold>DO NOT</bold> use an existing workspace
    * here, otherwise you will get concurrent access affects.
    * 
    * @parameter expression="${emfgen.workspace}"
    *            default-value="${basedir}/target/workspace"
    * @required
    */
   private String workspace = null;
   /**
    * Output directory
    * 
    * @parameter expression="${emfgen.output.dir}"
    *            default-value="${basedir}/target/genmodel"
    * @required
    */
   private String outputDirectory = null;
   /**
    * Name of the genmodel file to generate
    * 
    * @parameter
    * @required
    */
   private String genModel = null;
   /**
    * @parameter expression="${project}"
    * @required
    * @readonly
    */
   private MavenProject project;
   /**
    * Time difference to treat as up-to-date.
    * 
    * @parameter expression="${emfgen.lastModGranularityMs}" default-value="10"
    * @required
    */
   private int staleMillis;

   /**
    * An array with the full names to the ecore files
    * 
    * @parameter
    * @required
    */
   private String metaModels[] = null;

   /**
    * Namespace URIs in the meta models
    * 
    * @parameter
    * @required
    */
   private String packageUris[] = null;

   /**
    * Path to the directory for the sources to be generated
    * 
    * @parameter default-value="${emfgen.output.dir}/src/main/java
    * @required
    */
   private String genSrc = null;

   /**
    * ID of the model plugin
    * 
    * @parameter
    */
   private String modelPluginID = null;

   /**
    * Copyright string
    * 
    * @parameter default-value="Copyright (C) 2006 Free Software Foundation"
    */
   private String copyrightString = null;

   /**
    * Namespace URI
    * 
    * @parameter
    */
   private String namespaceUri = null;

   /**
    * Base name, i.e. the root package, which contains the package given with
    * parameter <code>prefixName</code>. I use the domain name here.
    * 
    * @parameter
    */
   private String baseName = null;

   /**
    * Prefix name, i.e. a subpackage under the root package (parameter
    * <code>baseName</code>). I use <code>metamodel</code> here.
    * 
    * @parameter
    */
   private String prefixName = null;

   /**
    * Base dir
    * 
    * @parameter default-value="${basedir}"
    * @required
    * @readonly
    */
   private String basedir = null;

   /**
    * Eclipse version, e.g. <code>3.2.1</code> or <code>3.3.0</code>.  If not given, it tries to query the version itself.
    * 
    * @parameter
    */
   private String eclipseVersion;

   private static final String TIME_LOG_FILE_NAME = ".timelog";
   private static final Pattern evregexp = Pattern.compile("3\\.[2-4](\\.[0-9])?");

   public void setEclipseDir(String dir)
   {
      eclipseDir = dir;
   }

   public String getEclipseDir()
   {
      return eclipseDir;
   }

   public void setWorkspace(String path)
   {
      workspace = path;
   }

   public String getWorkspace()
   {
      return workspace;
   }

   public void setOutputDirectory(String dir)
   {
      outputDirectory = dir;
   }

   public String getOutputDirectory()
   {
      return outputDirectory;
   }

   public void setGenModel(String path)
   {
      genModel = path;
   }

   public String getGenModel()
   {
      return outputDirectory + '/' + genModel;
   }

   public MavenProject getProject()
   {
      return project;
   }

   public File getTimeLogFile()
   {
      return new File(getOutputDirectory(), TIME_LOG_FILE_NAME);
   }

   public static void touchFile(File file) throws MojoExecutionException
   {
      if (file.exists()) file.setLastModified(System.currentTimeMillis());
      else try
      {
         file.getParentFile().mkdirs();
         file.createNewFile();
      }
      catch (IOException e)
      {
         throw new MojoExecutionException("Cannot create timelog file.", e);
      }
   }

   public void execute() throws MojoExecutionException, MojoFailureException
   {
      if (shouldRun())
      {
         if (StringUtils.isEmpty(getEclipseVersion()) || !evregexp.matcher(getEclipseVersion()).matches()) discoverEclipseVersion();
         
         File genSrc = new File(getOutputDirectory(), getGenSrc());
         if (!genSrc.exists())
         {
            if (!genSrc.mkdirs())
               throw new MojoFailureException("Cannot create output directory: " + genSrc.getPath());
         }
         try
         {
            Commandline cl = getJavaCmdln();
            cl.addArguments(getEcore2GenModelCmdln());

            if (getLog().isDebugEnabled()) getLog().debug("Running " + cl.toString());
            else if (getLog().isInfoEnabled())
               getLog().info("Generating GenModel.");
            CommandLineUtils.StringStreamConsumer stdout =
                  new CommandLineUtils.StringStreamConsumer(), stderr =
                  new CommandLineUtils.StringStreamConsumer();
            int returnval =
                  CommandLineUtils.executeCommandLine(cl, stdout, stderr);
            String outstr = stdout.getOutput(), errstr = stderr.getOutput();
            if (getLog().isInfoEnabled() && StringUtils.isNotEmpty(outstr))
               getLog().info(outstr);
            if (getLog().isErrorEnabled() && StringUtils.isNotEmpty(errstr))
               getLog().error(errstr);

            if (returnval == 0)
            {
               cl = getJavaCmdln();
               cl.addArguments(getEcoreGeneratorCmdln());

               if (getLog().isDebugEnabled()) getLog().debug("Running " + cl.toString());
               else if (getLog().isInfoEnabled())
                  getLog().info("Generating model sources.");
               returnval =
                     CommandLineUtils.executeCommandLine(cl, stdout, stderr);
               outstr = stdout.getOutput();
               errstr = stderr.getOutput();

               if (getLog().isInfoEnabled() && StringUtils.isNotEmpty(outstr))
                  getLog().info(outstr);
               if (getLog().isErrorEnabled() && StringUtils.isNotEmpty(errstr))
                  getLog().error(errstr);

               if (returnval != 0)
                  throw new MojoFailureException("EMF generator fails in step gensrc with return value " + returnval);
            }
            else throw new MojoFailureException("EMF Generator fails in step ecore2genmodel with return value " + returnval);
         }
         catch (Exception clx)
         {
            getLog().error(clx);
            throw new MojoExecutionException(clx.getMessage(), clx);
         }
      }
      else if (getLog().isInfoEnabled())
         getLog().info("Model is unchanged, skip generating.");

      if (getProject() != null)
         getProject().addCompileSourceRoot(outputDirectory);

   }

   private void discoverEclipseVersion() throws MojoExecutionException
   {
      Properties props = new Properties();
      FileInputStream fis = null;
      try
      {
         fis = new FileInputStream(new File(getEclipseDir() + File.separator + ".eclipseproduct"));
         props.load(fis);
         eclipseVersion = (String)props.get("version");
         getLog().info("Discovered Eclipse version " + eclipseVersion);
      }
      catch (FileNotFoundException e)
      {
         throw new MojoExecutionException("Eclipse installation not recognized", e);
      }
      catch (IOException e)
      {
         throw new MojoExecutionException(e.getMessage(),e);
      }
      finally {
         try
         {
            fis.close();
         }
         catch (IOException e)
         {
            throw new MojoExecutionException(e.getMessage(), e);
         }
      }
   }

   /**
    * @param cl
    * @throws Exception if an error occurs
    */
   private Commandline getJavaCmdln() throws Exception
   {
      Commandline cl = new Commandline();
      cl.setExecutable("java");
      cl.createArgument().setValue("-classpath");
      if (eclipseVersion.startsWith("3.2")) {
      cl.createArgument()
         .setValue(Commandline.quoteArgument(getEclipseDir() + "/startup.jar"));
      } else if (eclipseVersion.startsWith("3.3") || eclipseVersion.startsWith("3.4")) {
         List jars = FileUtils.getFileNames(new File(getEclipseDir() + File.separator + "plugins"), "org.eclipse.equinox.launcher_*.jar", "", true);
         if (jars == null || jars.isEmpty())
            throw new MojoFailureException("No launcher plugin found.");
         
         cl.createArgument().setValue(Commandline.quoteArgument(jars.get(0).toString()));
      } else 
            throw new MojoFailureException("Eclipse version "+eclipseVersion+" not yet supported.");
      cl.createArgument().setValue("org.eclipse.core.launcher.Main");
      cl.createArgument().setValue("-clean");
      cl.createArgument().setValue("-data");
      cl.createArgument().setValue(Commandline.quoteArgument(getWorkspace()));
      cl.addSystemEnvironment();
      cl.setWorkingDirectory(getBasedir());
      return cl;
   }

   public final int getStaleMillis()
   {
      return staleMillis;
   }

   public final void setStaleMillis(int staleMillis)
   {
      this.staleMillis = staleMillis;
   }

   public final String[] getMetaModels()
   {
      return metaModels;
   }

   public final void setMetaModels(String metaModels[])
   {
      this.metaModels = metaModels;
   }

   public final String getGenSrc()
   {
      return genSrc;
   }

   public final void setGenSrc(String genSrc)
   {
      this.genSrc = genSrc;
   }

   public final String getModelPluginID()
   {
      return modelPluginID;
   }

   public final void setModelPluginID(String modelPluginID)
   {
      this.modelPluginID = modelPluginID;
   }

   public final String getCopyrightString()
   {
      return copyrightString;
   }

   public final void setCopyrightString(String copyrightString)
   {
      this.copyrightString = copyrightString;
   }

   public final String getBaseName()
   {
      return baseName;
   }

   public final void setBaseName(String baseName)
   {
      this.baseName = baseName;
   }

   public final String getNamespaceUri()
   {
      return namespaceUri;
   }

   public final void setNamespaceUri(String namespaceUri)
   {
      this.namespaceUri = namespaceUri;
   }

   public final String getPrefixName()
   {
      return prefixName;
   }

   public final void setPrefixName(String prefixName)
   {
      this.prefixName = prefixName;
   }

   protected String[] getEcore2GenModelCmdln() throws CommandLineException
   {
      List<String> args = new ArrayList<String>(7);
      args.add("-application");
      args.add("org.eclipse.emf.importer.ecore.Ecore2GenModel");
      for (String model: getMetaModels())
      {
         args.add(Commandline.quoteArgument(model));
      }
      args.add(Commandline.quoteArgument(getGenModel()));
      for (String packageUri: getPackageUris())
      {
         args.add("-package");
         args.add(Commandline.quoteArgument(packageUri));
      }
      args.add("-modelProject");
      args.add(Commandline.quoteArgument(getOutputDirectory()));
      args.add(Commandline.quoteArgument(getGenSrc()));
      if (StringUtils.isNotEmpty(getModelPluginID()))
      {
         args.add("-modelPluginID");
         args.add(Commandline.quoteArgument(getModelPluginID()));
      }
      if (StringUtils.isNotEmpty(getCopyrightString()))
      {
         args.add("-copyright");
         args.add(getCopyrightString());
      }
      if (StringUtils.isNotEmpty(getNamespaceUri()))
      {
         args.add("-package");
         args.add(getNamespaceUri());
         if (StringUtils.isNotEmpty(getBaseName()) && StringUtils.isNotEmpty(getPrefixName()))
         {
            args.add(getBaseName());
            args.add(getPrefixName());
         }
      }

      return (String[])args.toArray(new String[0]);
   }

   protected String[] getEcoreGeneratorCmdln() throws CommandLineException
   {
      List<String> args = new ArrayList<String>(4);
      args.add("-application");
      args.add("org.eclipse.emf.codegen.ecore.Generator");
      args.add("-projects");
      args.add(Commandline.quoteArgument(getOutputDirectory()));
      args.add("-model");
      args.add(Commandline.quoteArgument(getGenModel()));
      return args.toArray(new String[0]);
   }

   protected boolean shouldRun() throws MojoFailureException,
                                MojoExecutionException
   {
      boolean shouldRun = true;
      File timeLogFile = getTimeLogFile();
      File genModelFile = new File(getGenModel());
      if (genModelFile.exists() && timeLogFile.exists())
      {
         long lm_tlf = timeLogFile.lastModified();
         long lm_gmf = genModelFile.lastModified();

         for (String metaModelFileName: getMetaModels())
         {
            File metaModelFile = new File(metaModelFileName);
            if (!metaModelFile.exists())
               throw new MojoFailureException("Metamodel file " + metaModelFileName
                                              + " doesn't exist.");
            long lm_mmf = metaModelFile.lastModified();
            shouldRun =
                  lm_mmf - lm_tlf > getStaleMillis() || lm_mmf - lm_gmf > getStaleMillis();
            if (shouldRun) break;
         }

      }

      touchFile(timeLogFile);
      return shouldRun;
   }

   protected void prepareExecute() throws MojoFailureException
   {
      File genSrc = new File(getOutputDirectory(), getGenSrc());
      if (!genSrc.exists())
      {
         if (!genSrc.mkdirs())
            throw new MojoFailureException("Cannot create output directory: " + genSrc.getPath());
      }
   }

   public final String getBasedir()
   {
      return basedir;
   }

   public final void setBasedir(String basedir)
   {
      this.basedir = basedir;
   }

   /**
    * @return the packageUris
    */
   public final String[] getPackageUris()
   {
      return packageUris;
   }

   /**
    * @param packageUris
    *           the packageUris to set
    */
   public final void setPackageUris(String[] packageUris)
   {
      this.packageUris = packageUris;
   }

   /**
    * @return the eclipseVersion
    */
   public final String getEclipseVersion()
   {
      return eclipseVersion;
   }

   /**
    * 
    * @param eclipseVersion the eclipseVersion to set
    */
   public final void setEclipseVersion(String eclipseVersion)
   {
      this.eclipseVersion = eclipseVersion;
   }

}
