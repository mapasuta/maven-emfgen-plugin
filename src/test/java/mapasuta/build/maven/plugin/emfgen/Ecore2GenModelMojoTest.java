/**
 * Mapasuta
 *
 * Copyright (C) 2006 Dominik Winter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 **/
package mapasuta.build.maven.plugin.emfgen;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.testng.annotations.*;

/**
 * @author Dominik Winter
 */
public final class Ecore2GenModelMojoTest
{
   private EmfGenMojo mojo = null;

   @Parameters({"ecoreModel", "genModel"})
   public Ecore2GenModelMojoTest(String ecoreModel, String genModel)
   {
      mojo = new EmfGenMojo();
      mojo.setMetaModels(new String[]{"./" + ecoreModel});
      mojo.setPackageUris(new String[]{"http://www.openarchitectureware.org/oaw4.demo.emf.datamodel"});
      mojo.setEclipseDir(System.getProperty("eclipse.home"));
      mojo.setWorkspace(System.getProperty("eclipse.workspace"));
      mojo.setOutputDirectory(System.getProperty("output.dir"));
      mojo.setGenModel(genModel);
      mojo.setGenSrc("src/main/java");
      mojo.setModelPluginID("test.model");
      mojo.setBasedir(System.getProperty("basedir"));
   }

   @Test
   public final void execute() throws MojoExecutionException,
                              MojoFailureException
   {
      mojo.execute();
   }
}
